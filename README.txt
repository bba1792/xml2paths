This tool prints all valid pathes for XML files. The first and only required
parameter is either the path of an XML file to be processed or of a folder. 
In the second case all files found within the folder will be processed. By
default all files with the extension XML will be processed. The second
parameter can overwrite this by explicitly stating a list of file extensions
to be processed. Another parameter '-count' chages the behaviour that
multiple occurences of the same path are not repeated but instead their
count is summed up and printed as well (same as piping the output through 'sort | uniq
-c | sort -n').